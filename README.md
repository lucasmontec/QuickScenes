# Quick Scenes - scene navigation

Quick Scenes is a free scene organization package to help organizing unity projects. Its a window that shows buttons to scene files so you can navigate to your working set of scenes directly. Scenes are added automatically to this working set when you open a new scene.

## Installation

### UPM
Click to add a git package and past this URL: `https://gitlab.com/lucasmontec/QuickScenes.git?path=Assets/Plugins/SawceGames/com.sawcegames.quickscenes`

### Asset Store
https://assetstore.unity.com/packages/tools/utilities/quick-scenes-104198
