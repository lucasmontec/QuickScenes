using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using QuickScenes.Settings;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace QuickScenes
{
    public class ScenesWindow : EditorWindow, IHasCustomMenu
    {
        private struct SceneInfo
        {
            public string Name;
            public string Path;
        }
        
        /// <summary>
        /// Current active window.
        /// </summary>
        private static ScenesWindow current;

        /// <summary>
        /// Scene name character limit.
        /// </summary>
        private int _maxSceneName = 32;

        /// <summary>
        /// Current scene names.
        /// </summary>
        private readonly List<SceneInfo> _scenes = new();

        /// <summary>
        /// Current scene names on display (can be filtered).
        /// </summary>
        private readonly List<SceneInfo> _filteredScenes = new();

        /// <summary>
        /// Points scene path to their names for all scenes.
        /// </summary>
        private readonly Dictionary<string, SceneInfo> _allScenesMap = new();

        /// <summary>
        /// Currently selected button grid element.
        /// </summary>
        private int _selectedGridIndex = -1;

        /// <summary>
        /// This holds all GUID to all projects scenes when loaded.
        /// </summary>
        private string[] _allScenes;

        /// <summary>
        /// Remove the scene next click instead of going to it.
        /// </summary>
        private bool _removeNextClick;

        /// <summary>
        /// If no scenes are found when filtering the scene set, but are found when searching all scenes,
        /// next click will add the scene.
        /// </summary>
        private bool _addNextClick;

        /// <summary>
        /// When the user holds the alt key, this is set to true
        /// preventing scenes to be added.
        /// </summary>
        private bool _dontAddScene;

        /// <summary>
        /// When the user holds the shift key, this is set to true
        /// preventing scene navigation.
        /// </summary>
        private bool _dontNavigateToScene;

        private Vector2 _sceneButtonsScrollPos;

        /// <summary>
        /// Variable to store the last filter used in the last filter operation.
        /// Used to prevent constant re filtering.
        /// </summary>
        private string _lastFilter;

        /// <summary>
        /// This is set to true if the current filter text contains the '*' character.
        /// </summary>
        private bool _filterIsSearch;

        /// <summary>
        /// Debounce bool for remove scenes toggle.
        /// Set to true until the user releases ctrl.
        /// </summary>
        private bool _toggledRemoveScenes;

        private QuickScenesSettings _settings;

        private QuickScenesSettings Settings => _settings ??= QuickScenesSettings.GetOrCreateSettings();
        
        public ScenesWindow()
        {
            _scenes.Clear();
            _filteredScenes.Clear();

            current = this;
        }

        #region OPTIONS

        private string _filter = "";

        #endregion OPTIONS

        #region SETTINGS

        private static bool detectSceneChanges = true;
        private readonly GUIContent _detectSceneChangesItem = new("Detect Scene Changes");

        private readonly GUIContent _addAllScenesItem = new("Add all scenes");
        private readonly GUIContent _addBuildEnabledScenesItem = new("Add build scenes");
        private readonly GUIContent _clearAllScenesItem = new("Clear all scenes");
        private readonly GUIContent _openSettingsItem = new("Open Settings");

        /// <summary>
        /// Adds a custom window context menu.
        /// </summary>
        /// <param name="menu"></param>
        public void AddItemsToMenu(GenericMenu menu)
        {
            detectSceneChanges = EditorPrefs.GetBool(EDITOR_SAVE_KEY + "detectScenes", true);

            menu.AddItem(_detectSceneChangesItem, detectSceneChanges, DetectMapToggle);
            menu.AddItem(_addAllScenesItem, false, AddAllScenes);
            menu.AddItem(_addBuildEnabledScenesItem, false, AddEnabledScenesFromBuild);
            menu.AddItem(_clearAllScenesItem, false, ClearAllScenes);
            menu.AddItem(_openSettingsItem, false, OpenSettings);
        }

        private static void OpenSettings()
        {
            SettingsService.OpenProjectSettings("Project/Quick Scenes");
        }

        private static void DetectMapToggle()
        {
            detectSceneChanges = !detectSceneChanges;
            EditorPrefs.SetBool(EDITOR_SAVE_KEY + "detectScenes", detectSceneChanges);
        }

        private void ClearAllScenes()
        {
            _scenes.Clear();
            _filteredScenes.Clear();
            
            _allScenesMap.Clear();
            
            SaveAndSetDirty();
        }

        /// <summary>
        /// Adds all scenes in the project to the button menu.
        /// </summary>
        private void AddAllScenes()
        {
            FindAllScenes();
            
            foreach (string guid in _allScenes)
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);
                string sceneFileName = Path.GetFileName(path)?.Replace(".unity", "");

                if (string.IsNullOrEmpty(sceneFileName)) continue;
                if(_scenes.Exists(s => s.Path == path)) continue;
                
                _scenes.Add(new SceneInfo
                {
                    Name = sceneFileName,
                    Path = path
                });
            }

            SaveAndSetDirty();
            BuildAllScenesMap();

            _lastFilter = null;
        }
        
        /// <summary>
        /// Adds all enabled scenes from the build settings to the button menu.
        /// </summary>
        private void AddEnabledScenesFromBuild()
        {
            var scenesInBuild = EditorBuildSettings.scenes
                .Where(scene => scene.enabled).Select(scene => scene.path).ToList();

            foreach (string path in scenesInBuild)
            {
                string sceneFileName = Path.GetFileName(path)?.Replace(".unity", "");

                if (string.IsNullOrEmpty(sceneFileName)) continue;
                if (_scenes.Exists(s => s.Path == path)) continue;

                _scenes.Add(new SceneInfo
                {
                    Name = sceneFileName,
                    Path = path
                });
            }

            SaveAndSetDirty();
            BuildAllScenesMap();

            _lastFilter = null;
        }


        #endregion SETTINGS

        #region STYLE

        private static GUIStyle centerBoldTextStyle;
        private static GUIStyle textFieldStyle;
        private static GUIStyle sceneButtonStyle;
        private static readonly Color light = new(0.6f, 0.6f, 0.6f);
        private static readonly Color lightRed = new(0.65f, 0.15f, 0.15f);
        private static readonly Color lightGreen = new(0.15f, 0.55f, 0.15f);

        #endregion STYLE

        #region SAVE AND LOAD
        
        private const string EDITOR_SAVE_KEY = "quickscenes_";
        private const string QUICKSCENES_SCENES_JSON = "QuickScenes/{name}.json";
        private const string SCENES_KEY = "current_scenes";

        #endregion SAVE AND LOAD

        [MenuItem("Window/Quick Scenes")]
        public static void ShowWindow()
        {
            GetWindow(typeof(ScenesWindow));
        }

        /// <summary>
        /// Accept drops anywhere.
        /// </summary>
        private void AcceptDrops()
        {
            Event evt = Event.current;

            DragAndDrop.visualMode = DragAndDropVisualMode.Copy;

            if (evt.type != EventType.DragPerform) return;
            DragAndDrop.AcceptDrag();

            foreach (UnityEngine.Object draggedObject in DragAndDrop.objectReferences)
            {
                SceneAsset scene = draggedObject as SceneAsset;

                if (scene == null) continue;
                string path = AssetDatabase.GetAssetPath(scene.GetInstanceID());
                _scenes.Add(new SceneInfo() 
                {
                    Name = scene.name,
                    Path = path
                });
            }

            if(!_allScenesMap.Any()) 
                BuildAllScenesMap();
            
            SaveAndSetDirty();
            
            //Necessary to cause filtering
            _lastFilter = null;
        }

        /// <summary>
        /// Builds all buttons styles
        /// </summary>
        private static void ValidateStyle()
        {
            if (sceneButtonStyle != null) return;

            //Default
            sceneButtonStyle = new GUIStyle(GUI.skin.button)
            {
                fontSize = 11,
                wordWrap = true,
                margin = new RectOffset(3, 3, 3, 3),
                padding = new RectOffset(15, 15, 15, 15),
                alignment = TextAnchor.MiddleCenter,
                clipping = TextClipping.Overflow,
                normal = {textColor = new Color(1, 1, 1)},
                fontStyle = FontStyle.Bold
            };

            textFieldStyle = new GUIStyle(GUI.skin.textField)
            {
                margin = new RectOffset(5, 5, 4, 3)
            };
            
            centerBoldTextStyle = new GUIStyle(GUI.skin.label)
            {
                alignment = TextAnchor.MiddleCenter,
                fontStyle = FontStyle.Bold
            };
        }

        /// <summary>
        /// Saves current scenes and set editor dirty.
        /// </summary>
        private void SaveAndSetDirty()
        {
            //Save scenes
            SaveScenes();
            
            EditorUtility.SetDirty(this);
        }

        private void PrepareWindowTitle()
        {
            // Loads an icon from an image stored at the specified path
            Texture icon = GetIcon();

            // Create the instance of GUIContent to assign to the window. Gives the title "RBSettings" and the icon
            GUIContent content = new("Quick Scenes", icon);
            titleContent = content;
        }

        private static Texture GetIcon()
        {
            Texture icon = AssetDatabase.LoadAssetAtPath<Texture>("Assets/Plugins/SawceGames/QuickScenes/logo.png");
            icon ??= AssetDatabase.LoadAssetAtPath<Texture>("Assets/Plugins/SawceGames/com.sawcegames.quickscenes/logo.png");
            icon ??= AssetDatabase.LoadAssetAtPath<Texture>("Packages/com.sawcegames.quickscenes/logo.png");
            return icon;
        }

        private void OnEnable()
        {
            _settings = QuickScenesSettings.GetOrCreateSettings();
            
            PrepareWindowTitle();
            
            LoadScenes();

            SaveAndSetDirty();

            FindAllScenes();
            BuildAllScenesMap();

            EditorSceneManager.sceneOpened += OnSceneOpened;
            EditorSceneManager.newSceneCreated += OnNewScene;
            
            //Reset state
            _lastFilter = null;
        }

        /// <summary>
        /// Loads the scenes list saved on the system.
        /// </summary>
        private void LoadScenes()
        {
            _scenes.Clear();
            
            var loadedScenes = ReadSerializableObject<List<SceneInfo>>(SCENES_KEY);
            if(loadedScenes != null)
                _scenes.AddRange(loadedScenes);
        }
        
        /// <summary>
        /// Saves scenes list.
        /// </summary>
        private void SaveScenes()
        {
            SaveSerializableObject(SCENES_KEY, _scenes);
        }

        private void OnNewScene(Scene scene, NewSceneSetup setup, NewSceneMode mode)
        {
            FindAllScenes();
            BuildAllScenesMap();
            _lastFilter = null;
        }

        private void OnDisable()
        {
            EditorSceneManager.sceneOpened -= OnSceneOpened;
            SaveScenes();
        }

        /// <summary>
        /// Automatic scene change addition.
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="mode"></param>
        private void OnSceneOpened(Scene scene, OpenSceneMode mode)
        {
            if (!detectSceneChanges && !_addNextClick) return;
            
            if (!AddScene(scene)) return;
            if(!_allScenesMap.Any()) BuildAllScenesMap();
            SaveAndSetDirty();
            
            //Necessary to cause filtering
            _lastFilter = null;
        }

        /// <summary>
        /// Adds a scene to the current scenes list.
        /// Doesn't add the scene if there is a scene with the same path added.
        /// IF Don't add scene is set, this is also ignored.
        /// </summary>
        /// <param name="scene"></param>
        /// <returns>true if the scene was added</returns>
        private bool AddScene(Scene scene)
        {
            if (_scenes.Exists(s => s.Path == scene.path)) return false;
            if (_dontAddScene) return false;
            
            _scenes.Add(new SceneInfo
            {
                Name = scene.name,
                Path = scene.path
            });
            return true;
        }

        /// <summary>
        /// Applies filter and filter search.
        /// </summary>
        private void FilterScenes()
        {
            if (_allScenesMap.Count == 0)
            {
                BuildAllScenesMap();
            }
            
            //Reset
            _filterIsSearch = false;
            
            //Prevent filtering forever
            if (_lastFilter == _filter) return;
            _lastFilter = _filter;
            
            //Reset
            _addNextClick = false;
            
            bool hasFilter = !string.IsNullOrEmpty(_filter);
            if (hasFilter)
            {
                //Finds scenes that contain the filter part
                _filteredScenes.Clear();
                _filteredScenes.AddRange(_scenes
                    .Where(s => s.Name.ToLower().Contains(_filter.ToLower()))
                    .ToArray()
                );

                _filterIsSearch = _filter.Contains("*");
                
                //If no scenes contain the filter, find all and filter on them
                bool noScenesFound = _filteredScenes.Count == 0;
                if (noScenesFound || _filterIsSearch)
                {
                    DoSearchFiltering();
                }
            }
            else
            {
                _filteredScenes.Clear();
                _filteredScenes.AddRange(_scenes);
            }
        }

        private void DoSearchFiltering()
        {
            _filteredScenes.Clear();
            _filteredScenes.AddRange(
                _allScenesMap.Values
                    .Where(s => s.Name.ToLower().Contains(_filter.Replace("*", "").ToLower()))
                    .ToArray()
            );

            if (_filteredScenes.Count <= 0)
            {
                return;
            }
            _removeNextClick = false;
            _addNextClick = true;
        }

        #region GLOBAL KEY PRESS DETECTION

        [InitializeOnLoadMethod]
        private static void EditorInit()
        {
            var info =
                typeof(EditorApplication)
                    .GetField("globalEventHandler",
                        System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.NonPublic);

            if (info == null) return;
            var value = (EditorApplication.CallbackFunction) info.GetValue(null);

            value += EditorGlobalKeyPress;

            info.SetValue(null, value);
        }

        private static void EditorGlobalKeyPress()
        {
            if (current == null)
            {
                return;
            }
            current.HandleKeyAndMouseInput();
        }
        
        private void HandleKeyAndMouseInput()
        {
            bool notRepaintEvent = Event.current == null || 
                                   Event.current.type == EventType.Repaint;
            if (notRepaintEvent)
            {
                return;
            }
    
            _dontAddScene = Event.current.alt;
            _dontNavigateToScene = Event.current.shift;
            _removeNextClick = Event.current.control;

            Repaint();
        }

        #endregion GLOBAL KEY PRESS DETECTION

        private void OnGUI()
        {
            ValidateStyle();
            AcceptDrops();

            DrawFilterField();

            //Does the scene filtering logic
            FilterScenes();
            
            DrawScenesSelectionUI();

            DrawActionButtons();

            if (!_addNextClick || _scenes.Count == 0)
            {
                EditorGUILayout.EndHorizontal();
            }

            DrawTipLabels();
        }

        private void DrawScenesSelectionUI()
        {
            bool anyScenesToDisplay = _scenes.Count > 0 || _filteredScenes.Any();
            if (anyScenesToDisplay)
                EditorGUILayout.BeginVertical("Box");
            else
                EditorGUILayout.BeginVertical();

            //Scroll layout if needed
            int count = 1;
            if (_filteredScenes.Any())
                count = Mathf.CeilToInt(_filteredScenes.Count / 3f);
            int height = (count > 1 ? 48 : 49) * count;

            //Min height if at least one scene
            if (_filteredScenes.Any())
            {
                height = Mathf.Max(50, height);
                height = Mathf.Min(250, height);
            }

            HandleKeyAndMouseInput();

            if (anyScenesToDisplay)
            {
                using var scrollView = new EditorGUILayout.ScrollViewScope(
                    _sceneButtonsScrollPos,
                    false,
                    false,
                    GUILayout.Height(height));
                _sceneButtonsScrollPos = scrollView.scrollPosition;
                DrawButtonGrid();
                ExecuteButtonGridAction();
            }

            EditorGUILayout.EndVertical();

            if (!_addNextClick || _scenes.Count == 0)
                EditorGUILayout.BeginHorizontal("Box");
        }

        private void ExecuteButtonGridAction()
        {
            if (_selectedGridIndex < 0) return;
            SceneButtonClicked();

            //Reset the last filter, so we can go back to the scene listing
            _lastFilter = null;
            //Reset the selected scene
            _selectedGridIndex = -1;
        }

        private void DrawFilterField()
        {
            using (new EditorGUILayout.HorizontalScope("Box"))
            {
                _filter = EditorGUILayout.TextField("Filter:", _filter, textFieldStyle);

                GUI.enabled = !string.IsNullOrEmpty(_filter);
                
                if (GUILayout.Button(EditorGUIUtility.IconContent("d_TreeEditor.Trash"), GUILayout.MaxWidth(40)))
                {
                    ClearFilter();
                }

                GUI.enabled = true;
            }
        }

        private void ClearFilter()
        {
            GUI.FocusControl(null);
            _filter = "";
        }

        private void DrawActionButtons()
        {
            if (_scenes.Count > 0)
            {
                return;
            }

            using (new EditorGUILayout.VerticalScope())
            {
                GUILayout.FlexibleSpace();
                GUILayout.Label("Drop scenes here!", centerBoldTextStyle);
                GUILayout.FlexibleSpace();
                
                if (GUILayout.Button("Add all scenes", GUILayout.Height(50), GUILayout.MinHeight(25)))
                {
                    AddAllScenes();
                }
                _removeNextClick = false;
            }
        }

        private void DrawTipLabels()
        {
            if (_dontNavigateToScene)
            {
                EditorGUILayout.LabelField("Shift being held. Won't navigate to the scene when clicked.");
            }

            if (_dontAddScene)
            {
                EditorGUILayout.LabelField("Alt being held. Won't add the scene to the current set.");
            }

            if (_filterIsSearch)
            {
                EditorGUILayout.LabelField("Filter contains '*'. All scenes will be searched. Search mode is locked.");
            }
        }

        private void DrawButtonGrid()
        {
            Color previousGUIColor = GUI.backgroundColor;
            SetGridColor();
            
            float maxButtonWidth = position.width - 20;
            
            CalculateMaxSceneNameLength();

            int columns = Settings.sceneGridButtonColumns;
            
            _selectedGridIndex = GUILayout.SelectionGrid(
                _selectedGridIndex, ConvertToGuiContent(_filteredScenes), columns,
                sceneButtonStyle, GUILayout.MaxWidth(maxButtonWidth), GUILayout.MinWidth(200));

            GUI.backgroundColor = previousGUIColor;
        }

        private void CalculateMaxSceneNameLength()
        {
            int smallGridNameSize = position.width < 260 ? 8 : 16;
            int mediumOrGridNameSize = position.width < 310 ? smallGridNameSize : 32;
            _maxSceneName = mediumOrGridNameSize;
        }

        private void SetGridColor()
        {
            GUI.backgroundColor = light;
            if (_removeNextClick)
            {
                GUI.backgroundColor = lightRed;
            }
            else
            {
                if (_addNextClick && !_dontAddScene)
                {
                    GUI.backgroundColor = lightGreen;
                }
            }
        }

        private void SceneButtonClicked()
        {
            //If there is nothing to remove, prevent removal
            if (_removeNextClick && _scenes.Count == 0)
            {
                _removeNextClick = false;
            }
 
            SceneInfo scene = _filteredScenes[_selectedGridIndex];
            
            //Check if scene still exists, if it doesn't remove it
            bool sceneNotPresent = false;
            if (!File.Exists(scene.Path))
            {
                sceneNotPresent = true;
                Debug.LogWarning($"The scene at {scene.Path} was removed.");
            }
            
            Event currentEvent = Event.current;
            bool isRightClick = currentEvent.button == 1;
            if (isRightClick)
            {
                EditorGUIUtility.PingObject(AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(scene.Path));
                return;
            }
            
            if (_removeNextClick || sceneNotPresent)
            {
                _scenes.Remove(scene);
                SaveAndSetDirty();
            }
            else
            {
                AddAndNavigateToScene(scene);
            }

            _selectedGridIndex = -1;
            if (Settings.clearFilterOnSceneSelect)
            {
                ClearFilter();
            }
        }

        private void AddAndNavigateToScene(SceneInfo scene)
        {
            if (_allScenesMap.TryGetValue(scene.Path, out var sceneInfo) &&
                !_scenes.Contains(sceneInfo))
            {
                _scenes.Add(sceneInfo);
                SaveAndSetDirty();
            }

            //If can navigate
            if (_dontNavigateToScene)
            {
                return;
            }

            if (SceneManager.GetActiveScene().path == scene.Path)
            {
                return;
            }

            //Check save
            if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
            {
                //If filter mode, open from the all scenes map
                EditorSceneManager.OpenScene(
                    _addNextClick
                        ? sceneInfo.Path
                        : scene.Path, OpenSceneMode.Single);
            }
        }

        /// <summary>
        /// Finds all scenes in the project and stores them
        /// to the local allScenes array.
        /// </summary>
        private void FindAllScenes()
        {
            _allScenes = AssetDatabase.FindAssets("t:Scene");
        }

        /// <summary>
        /// Builds the allScenesMap dictionary.
        /// For each asset GUID in the all scenes string array, this
        /// method finds the scene path, finds the file name and removes the .unity from it
        /// adding it to the sceneFilename.
        /// If the resulting file name is not empty, it is added to the all scenes map mapping:
        /// 
        /// scene path -> sceneInfo
        ///
        /// </summary>
        private void BuildAllScenesMap()
        {
            if(_allScenes == null || _allScenes.Length == 0)
                FindAllScenes();
            
            foreach (string guid in _allScenes!)
            {
                string path = AssetDatabase.GUIDToAssetPath(guid);
                string sceneFilename = Path.GetFileName(path)?.Replace(".unity", "");

                if (string.IsNullOrEmpty(sceneFilename)) continue;
                if (!_allScenesMap.ContainsKey(sceneFilename))
                {
                    _allScenesMap[path] = new SceneInfo
                    {
                        Name = sceneFilename,
                        Path = path
                    };
                }
            }
        }

        /// <summary>
        /// Limits each entry string length to maxSceneName characters or less.
        /// Converts each entry to a gui content with the scene name as text and
        /// scene path as tooltip.
        /// </summary>
        /// <param name="toLimit"></param>
        /// <returns></returns>
        private GUIContent[] ConvertToGuiContent(IEnumerable<SceneInfo> toLimit)
        {
            return toLimit
                .Select(v =>
                    new GUIContent
                    {
                        text = v.Name.Substring(0, Math.Min(v.Name.Length, _maxSceneName)),
                        tooltip = v.Path
                    }
                )
                .ToArray();
        }

        #region SERIALIZATION
        private static T ReadSerializableObject<T>(string objectName) where T : class
        {
            string currentFilePath = 
                QUICKSCENES_SCENES_JSON.Replace("{name}", objectName);

            if (!File.Exists(currentFilePath))
            {
                Debug.Log($"No such file: {currentFilePath}");
                return null;
            }

            string fileData = File.ReadAllText(currentFilePath);
            T obj = JsonConvert.DeserializeObject<T>(fileData);
            return obj;
        }
        
        private static void SaveSerializableObject<T>(string objectName, T toSave)
        {
            CheckAndCreateDirectory();

            string currentFilePath = QUICKSCENES_SCENES_JSON.Replace("{name}", objectName);
            
            //Remove current file if exists
            if (File.Exists(currentFilePath))
            {
                File.Delete(currentFilePath);
            }
            
            //Save list
            string serialized = JsonConvert.SerializeObject(toSave);
            File.WriteAllText(currentFilePath, serialized);
        }

        private static void CheckAndCreateDirectory()
        {
            if (!Directory.Exists("QuickScenes"))
            {
                Directory.CreateDirectory("QuickScenes");
            }
        }

        #endregion SERIALIZATION
    }
}
