using System;
using System.IO;
using System.Linq;
using QuickScenes.Settings;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityToolbarExtender;

namespace QuickScenes
{
    [InitializeOnLoad]
    public class CustomPlaymodeStartingSceneController
    {
        private const string PlayFromSceneIndexZero = "EditorPlayFromIndexZeroScene";
        private static Texture playButtonIcon;
        private static GUIContent sceneIconContent;
        private static QuickScenesSettings settings;
        private static QuickScenesSettings Settings => settings ??= QuickScenesSettings.GetOrCreateSettings();
        
        private static bool PlayFromBootScene
        {
            get => EditorPrefs.GetBool(PlayFromSceneIndexZero, false);
            set => EditorPrefs.SetBool(PlayFromSceneIndexZero, value);
        }
    
        static CustomPlaymodeStartingSceneController()
        {
            ToolbarExtender.RightToolbarGUI.Add(OnToolbarGUI);
            UpdatePlayModeScene();
        }

        private static void OnToolbarGUI()
        {
            if (Settings.playFromCustomScene)
            {
                DrawCustomSceneButton();
            }
            
            if (!Settings.showBootstrapPlaymodeSceneOption)
            {
                if (EditorSceneManager.playModeStartScene != null)
                {
                    EditorSceneManager.playModeStartScene = null;
                }
                return;
            }
            
            if (SceneManager.sceneCountInBuildSettings == 0)
            {
                return;
            }
            
            if (playButtonIcon == null)
            {
                playButtonIcon = EditorGUIUtility.IconContent("PlayButton").image;
            }

            sceneIconContent ??= EditorGUIUtility.IconContent("d_SceneAsset Icon");
            sceneIconContent.tooltip = "Toggle between playing from the current scene or the scene at build index 0.";

            DrawPlaySceneChangeButton();
        }

        private static void DrawCustomSceneButton()
        {
            string customSceneScenePath = Settings.customScene.ScenePath;
            if (string.IsNullOrEmpty(customSceneScenePath))
            {
                return;
            }
            
            var sceneName = Path.GetFileName(customSceneScenePath)
                .Replace(".unity", string.Empty);
            
            GUILayout.Space(5);
            if (GUILayout.Button(
                    new GUIContent(sceneName, playButtonIcon,
                        "Play from this scene."),
                    EditorStyles.toolbarButton))
            {
                var scene = AssetDatabase.LoadAssetAtPath<SceneAsset>(customSceneScenePath);
                EditorSceneManager.playModeStartScene = scene;
                EditorApplication.delayCall += () =>
                {
                    EditorApplication.isPlaying = true;
                };
            }

            if (Settings.showBootstrapPlaymodeSceneOption)
            {
                GUILayout.Space(5);
            }
            else
            {
                GUILayout.FlexibleSpace();
            }
        }
        
        private static void DrawPlaySceneChangeButton()
        {
            GUILayout.Space(5);

            string buttonText = PlayFromBootScene ? " Play from bootstrap" : " Play from current";
            sceneIconContent.text = buttonText;
            
            if (GUILayout.Button(sceneIconContent,
                    EditorStyles.toolbarButton, GUILayout.Width(140)))
            {
                var playFromBoot = PlayFromBootScene;
                playFromBoot = !playFromBoot;
                PlayFromBootScene = playFromBoot;

                UpdatePlayModeScene();
            }
            
            GUILayout.FlexibleSpace();
        }

        public static void UpdatePlayModeScene()
        {
            if (SceneManager.sceneCountInBuildSettings == 0)
            {
                Debug.Log("remove playmode scene");
                EditorSceneManager.playModeStartScene = null;
                return;
            }
            
            SceneAsset sceneAsset = GetBootstrapScene();
            EditorSceneManager.playModeStartScene =
                PlayFromBootScene ? sceneAsset : null;
        }

        private static SceneAsset GetBootstrapScene()
        {
            EditorBuildSettingsScene scene = Settings.bootstrapSceneType switch
            {
                BootstrapSceneType.FirstSceneOnBuildSettings => 
                    EditorBuildSettings.scenes[0],
                BootstrapSceneType.FirstEnabledSceneOnBuildSettings => 
                    EditorBuildSettings.scenes.FirstOrDefault(s => s.enabled),
                BootstrapSceneType.IndexOnBuildSettings => 
                    EditorBuildSettings.scenes[settings.bootstrapSceneIndex],
                _ => throw new ArgumentOutOfRangeException()
            };

            // ReSharper disable once InvertIf
            if (scene is null)
            {
                Debug.LogWarning("Failed to find a valid bootstrap scene! " +
                                 "The index provided might be wrong or no enabled scenes were present " +
                                 "while searching for the first enabled scene." +
                                 "Using the first scene on build settings...");
                scene = EditorBuildSettings.scenes[0];
            }
            
            return AssetDatabase.LoadAssetAtPath<SceneAsset>(scene.path);
        }
    }
}