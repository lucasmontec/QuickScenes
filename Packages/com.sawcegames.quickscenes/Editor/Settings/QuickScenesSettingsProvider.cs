using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace QuickScenes.Settings
{
    public class QuickScenesSettingsProvider : SettingsProvider
    {
        private class Styles
        {
            public static readonly GUIContent BootstrapSceneType = new("Bootstrap scene is ");
            public static readonly GUIContent BootstrapIndex = new("Bootstrap scene index ");
            public static readonly GUIContent ClearFilterWhenSceneClick = new("Clear filter when scene button clicked");
            public static readonly GUIContent CustomPlaymodeScene = new("Show bootstrap playmode scene option");
            public static readonly GUIContent QuickScenesWindowColumns = new("Scenes Window button columns");
            public static readonly GUIContent PlayFromCustomScene = new("Add play button to play from custom scene");
            public static readonly GUIContent CustomScene = new("Custom scene");
        }
        
        private SerializedObject _quickScenesSettings;

        private QuickScenesSettingsProvider(string path, SettingsScope scopes, IEnumerable<string> keywords = null) 
            : base(path, scopes, keywords)
        {
        }

        private static bool IsSettingsAvailable()
        {
            return File.Exists(SettingsConstants.SettingsPath);
        }
        
        public override void OnActivate(string searchContext, VisualElement rootElement)
        {
            _quickScenesSettings = QuickScenesSettings.GetSerializedSettings();
        }
        
        public override void OnGUI(string searchContext)
        {
            EditorGUIUtility.labelWidth = 250.0f;
            
            SerializedProperty clearFilterProperty = 
                _quickScenesSettings.FindProperty(nameof(QuickScenesSettings.clearFilterOnSceneSelect));
            
            SerializedProperty bootstrapPlayModeSceneProperty = 
                _quickScenesSettings.FindProperty(nameof(QuickScenesSettings.showBootstrapPlaymodeSceneOption));
            
            SerializedProperty sceneGridColumnsProperty = 
                _quickScenesSettings.FindProperty(nameof(QuickScenesSettings.sceneGridButtonColumns));
            
            SerializedProperty playFromCustomSceneProperty = 
                _quickScenesSettings.FindProperty(nameof(QuickScenesSettings.playFromCustomScene));

            using (new EditorGUILayout.VerticalScope("Box"))
            {
                DrawBootstrapSceneSettings();
                
                EditorGUILayout.PropertyField(clearFilterProperty, Styles.ClearFilterWhenSceneClick);
                EditorGUILayout.PropertyField(bootstrapPlayModeSceneProperty, Styles.CustomPlaymodeScene);
                EditorGUILayout.PropertyField(sceneGridColumnsProperty, Styles.QuickScenesWindowColumns);
                EditorGUILayout.PropertyField(playFromCustomSceneProperty, Styles.PlayFromCustomScene);

                DrawCustomSceneProperty();
            }

            _quickScenesSettings.ApplyModifiedProperties();
            ToolbarExtensions.RepaintToolbar();
            
            EditorGUIUtility.labelWidth = 0;
        }

        private void DrawBootstrapSceneSettings()
        {
            SerializedProperty bootstrapTypeProperty = 
                _quickScenesSettings.FindProperty(nameof(QuickScenesSettings.bootstrapSceneType));
            
            SerializedProperty bootstrapSceneIndex = 
                _quickScenesSettings.FindProperty(nameof(QuickScenesSettings.bootstrapSceneIndex));
            
            if (_quickScenesSettings.targetObject is not QuickScenesSettings settings) return;

            int originalType = bootstrapTypeProperty.intValue;
            EditorGUILayout.PropertyField(bootstrapTypeProperty, Styles.BootstrapSceneType);

            if (originalType != bootstrapTypeProperty.intValue)
            {
                EditorApplication.delayCall += CustomPlaymodeStartingSceneController.UpdatePlayModeScene;
            }

            if (settings.bootstrapSceneType != BootstrapSceneType.IndexOnBuildSettings)
            {
                return;
            }
            
            int originalIndex = bootstrapSceneIndex.intValue;
            
            EditorGUILayout.PropertyField(bootstrapSceneIndex, Styles.BootstrapIndex);
            int sceneCount = EditorBuildSettings.scenes.Length;
            bootstrapSceneIndex.intValue = Mathf.Clamp(bootstrapSceneIndex.intValue, 0, sceneCount - 1);
            
            if (originalIndex != bootstrapSceneIndex.intValue)
            {
                EditorApplication.delayCall += CustomPlaymodeStartingSceneController.UpdatePlayModeScene;
            }
        }

        private void DrawCustomSceneProperty()
        {
            SerializedProperty customSceneProperty = 
                _quickScenesSettings.FindProperty(nameof(QuickScenesSettings.customScene));

            if (_quickScenesSettings.targetObject is not QuickScenesSettings settings) return;
            
            if (settings.playFromCustomScene)
            {
                EditorGUILayout.PropertyField(customSceneProperty, Styles.CustomScene);
            }
        }

        [SettingsProvider]
        public static SettingsProvider CreateSettingsProvider()
        {
            if (!IsSettingsAvailable())
            {
                QuickScenesSettings.GetOrCreateSettings();
            }
            
            var provider = new QuickScenesSettingsProvider("Project/Quick Scenes", SettingsScope.Project)
             {
                 // Automatically extract all keywords from the Styles.
                 keywords = GetSearchKeywordsFromGUIContentProperties<Styles>()
             };

            return provider;
        }
    }
}