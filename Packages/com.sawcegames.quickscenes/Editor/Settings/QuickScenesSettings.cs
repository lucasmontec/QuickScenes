using System.IO;
using SawceGames.ThirdParty.SceneReference;
using UnityEditor;
using UnityEngine;

namespace QuickScenes.Settings
{
    public class QuickScenesSettings : ScriptableObject
    {
        public bool clearFilterOnSceneSelect;
        
        public bool showBootstrapPlaymodeSceneOption;
        [Min(1)]
        public int sceneGridButtonColumns = 3;

        public BootstrapSceneType bootstrapSceneType = 
            BootstrapSceneType.FirstEnabledSceneOnBuildSettings;

        public int bootstrapSceneIndex;
        
        public bool playFromCustomScene;
        public SceneReference customScene;
        
        public static QuickScenesSettings GetOrCreateSettings()
        {
            var settings = AssetDatabase.LoadAssetAtPath<QuickScenesSettings>(SettingsConstants.SettingsPath);
            if (settings != null) return settings;

            string settingsDirectory = Path.GetDirectoryName(SettingsConstants.SettingsPath);
            Directory.CreateDirectory(settingsDirectory!);
            
            settings = CreateInstance<QuickScenesSettings>();
            settings.clearFilterOnSceneSelect = true;
            
            //Initialize defaults
            AssetDatabase.CreateAsset(settings, SettingsConstants.SettingsPath);
            AssetDatabase.SaveAssets();
            return settings;
        }

        internal static SerializedObject GetSerializedSettings()
        {
            return new SerializedObject(GetOrCreateSettings());
        }
    }
}