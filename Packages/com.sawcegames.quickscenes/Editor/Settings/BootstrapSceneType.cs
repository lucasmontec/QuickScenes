namespace QuickScenes.Settings
{
    public enum BootstrapSceneType
    {
        FirstSceneOnBuildSettings,
        FirstEnabledSceneOnBuildSettings,
        IndexOnBuildSettings
    }
}