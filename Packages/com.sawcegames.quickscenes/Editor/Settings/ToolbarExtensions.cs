using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;

namespace QuickScenes.Settings
{
    public abstract class ToolbarExtensions
    {
        private static readonly Type toolbarType = typeof(Editor).Assembly.GetType("UnityEditor.Toolbar");

        public static void RepaintToolbar()
        {
            var toolbars = Resources.FindObjectsOfTypeAll(toolbarType);
            ScriptableObject currentToolbar = toolbars.Length > 0 ? (ScriptableObject) toolbars[0] : null;
            if (currentToolbar == null) return;
            var root = currentToolbar.GetType().GetField("m_Root", BindingFlags.NonPublic | BindingFlags.Instance);
            var rawRoot = root?.GetValue(currentToolbar);
            var mRoot = rawRoot as VisualElement;
            mRoot?.MarkDirtyRepaint();
        }
    }
}